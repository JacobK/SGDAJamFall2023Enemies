extends Area2D

"""
This is a spot where resources can be collected.
"""

var active = false

export(String) var resource_name = ""
export(int) var resource_num = 0
export(String) var resource_type = "active"
export(int) var hours = 1
var current_dialog = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	# warning-ignore:return_value_discarded
	connect("body_entered", self, "_on_body_entered")
	# warning-ignore:return_value_discarded
	connect("body_exited", self, "_on_body_exited")
	pass # Replace with function body.

func _input(event):
	# Bail if npc not active (player not inside the collider)
	if not active:
		return
	# Bail if Dialogs singleton is showing another dialog
	if Dialogs.active:
		return
	# Bail if the event is not a pressed "interact" action
	if not event.is_action_pressed("interact"):
		return
	# Bail if the resource does not exist
	if resource_name == "":
		return
	# Bail if the resource type is not active
	if resource_type != "active":
		return
	
	
	# If we reached here and there are resources left
	if resource_num > 0:
		print("There are %d instances of %s left." % [resource_num, resource_name])
		Dialogs.show_dialog("You got 1 %s" % resource_name, "")
		resource_num -= 1
		Inventory.add_item(resource_name, 1)


func _on_body_entered(body):
	if body is Player:
		active = true
		
func _on_body_exited(body):
	if body is Player:
		active = false

func _process(delta):
	pass
