extends Area2D

"""
This is a spot where resources can be collected.
"""

var active = false

export(String) var resource_name = ""
export(int) var resource_num = 0
export(String) var resource_type = "active"
var current_dialog = 0
export(int) var hours = 1
export(float) var percent_to_be_collected = 0.5
var people_working = 0
var cached_resources = 0

signal interface_started
signal interface_ended


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	# warning-ignore:return_value_discarded
	connect("body_entered", self, "_on_body_entered")
	# warning-ignore:return_value_discarded
	connect("body_exited", self, "_on_body_exited")
	pass # Replace with function body.

func _input(event):
	# Bail if npc not active (player not inside the collider)
	if not active:
		return
	# Bail if Dialogs singleton is showing another dialog
	if Dialogs.active:
		return
	# Bail if the event is not a pressed "interact" action
	if not event.is_action_pressed("interact"):
		return
	# Bail if the resource does not exist
	if resource_name == "":
		return
	# Bail if the resource type is not active
	if resource_type != "active":
		return
	
	
	# If we reached here and there are resources left
	if resource_num > 0:
		print("There are %d instances of %s left." % [resource_num, resource_name])
		Dialogs.show_dialog("Assigning 1 party member to collect %s" % resource_name, "")
		var resources_to_get = ceil(resource_num * percent_to_be_collected)
		resource_num -= resources_to_get
		people_working += 1
		cached_resources += resources_to_get
		GlobalTime.add_seconds_elapsed(hours * 60 * 60)
		Inventory.add_item(resource_name, cached_resources)
		cached_resources = 0
		
	else:
		Dialogs.show_dialog("There are no resources left here.", "")


func _on_body_entered(body):
	if body is Player:
		active = true
		
func _on_body_exited(body):
	if body is Player:
		active = false

func _process(delta):
	pass
