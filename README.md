<!--
SPDX-FileCopyrightText: 2023 Jacob K

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# SGDAJamFall2023Enemies

for https://itch.io/jam/sgda-fall-2023-game-jam

## Third Party Resources Used

["Top-down Action RPG Template" version 1.1.3](https://godotengine.org/asset-library/asset/487), licensed MIT, Copyright (c) 2019 Lisandro Lorea

["Godot 3 2D Day/Night Cycle" version 3.1.0](https://github.com/hiulit/Godot-3-2D-Day-Night-Cycle/releases/tag/v3.1.0), licensed MIT, Copyright (c) 2019 Xavier Gómez Gosálbez
